//Skladi�te �  Izbornik s opcijama : pregled skladi�ta po kategorijama(kategorije � za�titna odje�a i obu�a, alat, rezervni dijelovi),
//unos novih dijelova u skladi�te(opcije kategorija � unos dijelova u odre�enu kategoriju, 
//unositi naziv, �ifru, jm(KOM, PAR, KG, M, M2), stanje), pretra�ivanje elemenata, opcija za brisanje izlazak iz programa.


#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"skladiste_dijelova_za_vlakove_head.h"


void fProvjeraDatotekeZ(char* datotekaZ, unsigned int* pBrojDijelovaO)
{
	FILE* pDatotekaProvjeraO = fopen(datotekaZ, "rb");    //�itanje datoteke
	if (pDatotekaProvjeraO == NULL)
	{
		perror("Datoteka ne postoji");
		pDatotekaProvjeraO = fopen(datotekaZ, "wb");      //kreiranje datoteke
		if (pDatotekaProvjeraO == NULL)
		{
			perror("Datoteka se ne moze kreirati");                         //provjera datoteke zastitne opreme i odjece ako ne postoji, kreira se
			exit(EXIT_FAILURE);
		}
		else
		{
			fwrite(pBrojDijelovaO, sizeof(unsigned int), 1, pDatotekaProvjeraO);
			fclose(pDatotekaProvjeraO);
			printf("Datoteka kreirana\n");
		}
	}
	else
	{
		fread(pBrojDijelovaO, sizeof(unsigned int), 1, pDatotekaProvjeraO);
		fclose(pDatotekaProvjeraO);                                          //provjera ukoliko postoji
		printf("Datoteka postoji\n");
	}
}

void fProvjeraDatotekeA(char* datotekaA, unsigned int* pBrojDijelovaA)
{
	FILE* pDatotekaProvjeraA = fopen(datotekaA, "rb");
	if (pDatotekaProvjeraA == NULL)
	{
		perror("Datoteka ne postoji");
		pDatotekaProvjeraA = fopen(datotekaA, "wb");
		if (pDatotekaProvjeraA == NULL)
		{
			perror("Datoteka se ne moze kreirati");
			exit(EXIT_FAILURE);
		}
		else
		{
			fwrite(pBrojDijelovaA, sizeof(unsigned int), 1, pDatotekaProvjeraA);
			fclose(pDatotekaProvjeraA);
			printf("Datoteka kreirana\n");
		}
	}
	else
	{
		fread(pBrojDijelovaA, sizeof(unsigned int), 1, pDatotekaProvjeraA);
		fclose(pDatotekaProvjeraA);
		printf("Datoteka postoji\n");
	}
}

void fProvjeraDatotekeR(char* datotekaR, unsigned int* pBrojDijelovaR)
{
	FILE* pDatotekaProvjeraR = fopen(datotekaR, "rb");
	if (pDatotekaProvjeraR == NULL)
	{
		perror("Datoteka ne postoji");
		pDatotekaProvjeraR = fopen(datotekaR, "wb");
		if (pDatotekaProvjeraR == NULL)
		{
			perror("Datoteka se ne moze kreirati");
			exit(EXIT_FAILURE);
		}
		else
		{
			fwrite(pBrojDijelovaR, sizeof(unsigned int), 1, pDatotekaProvjeraR);
			fclose(pDatotekaProvjeraR);
			printf("Datoteka kreirana\n");
		}
	}
	else
	{
		fread(pBrojDijelovaR, sizeof(unsigned int), 1, pDatotekaProvjeraR);
		fclose(pDatotekaProvjeraR);
		printf("Datoteka postoji\n");
	}
}

void fDodajOpremu(char* datotekaZ, unsigned int* pBrojDijelovaO)
{
    FILE* pDodajO = NULL;
	pDodajO = fopen(datotekaZ, "rb+");                 // �itanje i zapisivanje u datoteku
	if (pDodajO == NULL)
	{
		perror("Izbornik 2 - Unos novih dijelova u skladiste");
		return;
	}
	else
	{
		DIJELOVI tempDioO = { 0 };
		printf("Unesite naziv: ");
		scanf(" %50[^\n]", tempDioO.naziv);
		printf("Unesite sifru: ");
		scanf("%u", &tempDioO.sifra);
		printf("Unesite jm: ");
		scanf("%10s", tempDioO.jm);
		printf("Unesite stanje: ");
		scanf("%d", &tempDioO.stanje);
		tempDioO.id = (*pBrojDijelovaO)++;
		
		fseek(pDodajO, sizeof(unsigned int) + ((*pBrojDijelovaO -1) * sizeof(DIJELOVI)), SEEK_SET);   //promjena polo�aja unutar datoteke od odredjene pozicije
		fwrite(&tempDioO, sizeof(DIJELOVI), 1, pDodajO);
		rewind(pDodajO);                //vracanje na pocetak sadrzaja datoteke
		fwrite(pBrojDijelovaO, sizeof(unsigned int), 1, pDodajO);
		fclose(pDodajO);
	}
}

void fDodajAlat(char* datotekaA, unsigned int* pBrojDijelovaA)
{
	FILE* pDodajA = NULL;
	pDodajA = fopen(datotekaA, "rb+");                 // �itanje i zapisivanje u datoteku
	if (pDodajA == NULL)
	{
		perror("Izbornik 2 - Unos novih dijelova u skladiste");
		return;
	}
	else
	{
		DIJELOVI tempDioA = { 0 };
		printf("Unesite naziv: ");
     	scanf(" %50[^\n]", tempDioA.naziv);
		printf("Unesite sifru: ");
		scanf("%u", &tempDioA.sifra);
		printf("Unesite jm: ");
		scanf("%10s", tempDioA.jm);
		printf("Unesite stanje: ");
		scanf("%d", &tempDioA.stanje);
		tempDioA.id = (*pBrojDijelovaA)++;

		fseek(pDodajA, sizeof(unsigned int) + ((*pBrojDijelovaA - 1) * sizeof(DIJELOVI)), SEEK_SET);
		fwrite(&tempDioA, sizeof(DIJELOVI), 1, pDodajA);
		rewind(pDodajA);
		fwrite(pBrojDijelovaA, sizeof(unsigned int), 1, pDodajA);
		fclose(pDodajA);
	}
}

void fDodajRezervu(char* datotekaR, unsigned int* pBrojDijelovaR)
{
	FILE* pDodajR = NULL;
	pDodajR = fopen(datotekaR, "rb+");                 // �itanje i zapisivanje u datoteku
	if (pDodajR == NULL)
	{
		perror("Izbornik 2 - Unos novih dijelova u skladiste");
		return;
	}
	else
	{
		DIJELOVI tempDioR = { 0 };
		printf("Unesite naziv: ");
		scanf(" %50[^\n]", tempDioR.naziv);
		printf("Unesite sifru: ");
		scanf("%u", &tempDioR.sifra);
		printf("Unesite jm: ");
		scanf("%10s", tempDioR.jm);
		printf("Unesite stanje: ");
		scanf("%d", &tempDioR.stanje);
		tempDioR.id = (*pBrojDijelovaR)++;

		fseek(pDodajR, sizeof(unsigned int) + ((*pBrojDijelovaR - 1) * sizeof(DIJELOVI)), SEEK_SET);
		fwrite(&tempDioR, sizeof(DIJELOVI), 1, pDodajR);
		rewind(pDodajR);
		fwrite(pBrojDijelovaR, sizeof(unsigned int), 1, pDodajR);
		fclose(pDodajR);
	}
}

void pregledOpreme(char* datotekaZ, unsigned int* pBrojDijelovaO)
{
	FILE* pDatotekaProcitajO = NULL;
	pDatotekaProcitajO = fopen(datotekaZ, "rb");
	if (pDatotekaProcitajO == NULL)
	{
		perror("Izbornik 1 - Pregled skladista");               //provjera datoteke
		return;
	}
	else
	{
		DIJELOVI* sviDijeloviO = NULL;
		fread(pBrojDijelovaO, sizeof(unsigned int), 1, pDatotekaProcitajO);
		if (*pBrojDijelovaO == 0)
		{                                                                            //citanje, provjera ima li unesenih dijelova
			printf("Nema unesenih dijelova.\n");
			fclose(pDatotekaProcitajO);
			return;
		}
		else
		{
			sviDijeloviO = (DIJELOVI*)calloc(*pBrojDijelovaO, sizeof(DIJELOVI));           //dinamicko zauzimanje memorije 
			if (sviDijeloviO == NULL)
			{
				perror("Citanje svih dijelova");                                          //provjera zauzeca memorije
				exit(EXIT_FAILURE);
			}
			else
			{
				fread(sviDijeloviO, sizeof(DIJELOVI), *pBrojDijelovaO, pDatotekaProcitajO);
				fclose(pDatotekaProcitajO);
				unsigned int i;                                                                        //citanje svih dijelova iz datoteke
				printf("NAZIV\t\t\tSIFRA\t\tJM\tSTANJE\n");
				for (i = 0; i < *pBrojDijelovaO; i++)                     
				{
					printf("%u. ", (sviDijeloviO + i)->id+1);
					printf("%s\t", (sviDijeloviO + i)->naziv);
					printf("%u\t", (sviDijeloviO+i)->sifra);
					printf("%s\t", (sviDijeloviO + i)->jm);
					printf("%d\t\n", (sviDijeloviO + i)->stanje);
				}
				free(sviDijeloviO);            //oslobadjanje zauzete memorije
			}
		}
	}
}

void pregledAlata(char* datotekaA, unsigned int* pBrojDijelovaA)
{
	FILE* pDatotekaProcitajA = NULL;
	pDatotekaProcitajA = fopen(datotekaA, "rb");
	if (pDatotekaProcitajA == NULL)
	{
		perror("Izbornik 1 - Pregled skladista");
		return;
	}
	else
	{
		DIJELOVI* sviDijeloviA = NULL;
		fread(pBrojDijelovaA, sizeof(unsigned int), 1, pDatotekaProcitajA);
		if (*pBrojDijelovaA == 0)
		{                                                                            //citanje, provjera ima li unesenih dijelova
			printf("Nema unesenih dijelova.\n");
			fclose(pDatotekaProcitajA);
			return;
		}
		else
		{
			sviDijeloviA = (DIJELOVI*)calloc(*pBrojDijelovaA, sizeof(DIJELOVI));
			if (sviDijeloviA == NULL)
			{
				perror("Citanje svih dijelova");
				exit(EXIT_FAILURE);
			}
			else
			{
				fread(sviDijeloviA, sizeof(DIJELOVI), *pBrojDijelovaA, pDatotekaProcitajA);
				fclose(pDatotekaProcitajA);
				unsigned int i;
				printf("NAZIV\t\t\tSIFRA\t\tJM\tSTANJE\n");
				for (i = 0; i < *pBrojDijelovaA; i++)
				{
					printf("%u. ", (sviDijeloviA + i)->id+1);
					printf("%s\t", (sviDijeloviA + i)->naziv);
					printf("%u\t", (sviDijeloviA + i)->sifra);
					printf("%s\t", (sviDijeloviA + i)->jm);
					printf("%d\t\n", (sviDijeloviA + i)->stanje);
				}
				free(sviDijeloviA);
			}
		}
	}
}

void pregledRezerve(char* datotekaR, unsigned int* pBrojDijelovaR)
{
	FILE* pDatotekaProcitajR = NULL;
	pDatotekaProcitajR = fopen(datotekaR, "rb");
	if (pDatotekaProcitajR == NULL)
	{
		perror("Izbornik 1 - Pregled skladista");
		return;
	}
	else
	{
		DIJELOVI* sviDijeloviR = NULL;
		fread(pBrojDijelovaR, sizeof(unsigned int), 1, pDatotekaProcitajR);
		if (*pBrojDijelovaR == 0)
		{                                                                            //citanje, provjera ima li unesenih dijelova
			printf("Nema unesenih dijelova.\n");
			fclose(pDatotekaProcitajR);
			return;
		}
		else
		{
			sviDijeloviR = (DIJELOVI*)calloc(*pBrojDijelovaR, sizeof(DIJELOVI));
			if (sviDijeloviR == NULL)
			{
				perror("Citanje svih dijelova");
				exit(EXIT_FAILURE);
			}
			else
			{
				fread(sviDijeloviR, sizeof(DIJELOVI), *pBrojDijelovaR, pDatotekaProcitajR);
				fclose(pDatotekaProcitajR);
				unsigned int i;
				printf("NAZIV\t\t\tSIFRA\t\tJM\tSTANJE\n");
				for (i = 0; i < *pBrojDijelovaR; i++)
				{
					printf("%u. ", (sviDijeloviR + i)->id+1);
					printf("%s\t", (sviDijeloviR + i)->naziv);
					printf("%u\t", (sviDijeloviR + i)->sifra);
					printf("%s\t", (sviDijeloviR + i)->jm);
					printf("%d\t\n", (sviDijeloviR + i)->stanje);
				}
				free(sviDijeloviR);
			}
		}
	}
}

void pretraziOpremu(char* datotekaZ, unsigned int* pBrojDijelovaO)
{
	FILE* pDatotekaProcitajO = NULL;
	pDatotekaProcitajO = fopen(datotekaZ, "rb");
	if (pDatotekaProcitajO == NULL)
	{
		perror("Izbornik 3 - Pregled skladista");
		return;
	}
	else
	{
		DIJELOVI* sviDijeloviO = NULL;
		fread(pBrojDijelovaO, sizeof(unsigned int), 1, pDatotekaProcitajO);
		if (*pBrojDijelovaO == 0)
		{                                                                            //citanje, provjera ima li unesenih dijelova
			printf("Nema unesenih dijelova.\n");
			fclose(pDatotekaProcitajO);
			return;
		}
		else
		{
			sviDijeloviO = (DIJELOVI*)calloc(*pBrojDijelovaO, sizeof(DIJELOVI));
			if (sviDijeloviO == NULL)
			{
				perror("Citanje svih dijelova");
				exit(EXIT_FAILURE);
			}
			else
			{
				fread(sviDijeloviO, sizeof(DIJELOVI), *pBrojDijelovaO, pDatotekaProcitajO);
				fclose(pDatotekaProcitajO);
				unsigned int i;
				char privremeniNaziv[51] = { '\0' };
				printf("Unesite trazeni dio: ");
				scanf(" %50[^\n]", privremeniNaziv);
				unsigned int statusPronalaska = 0;
				unsigned int indeksPronalaska = -1;
				for (i = 0; i < *pBrojDijelovaO; i++)                     //pretrazivanje po odre�enom nazivu
				{
					if (!strcmp((sviDijeloviO + i)->naziv, privremeniNaziv))
					{
						statusPronalaska = 1;
						indeksPronalaska = i;
					}
				}
				if (statusPronalaska)
				{
					printf("Dio pronadjen\n");
					printf("NAZIV\t\t\tSIFRA\t\tJM\tSTANJE\n");
					printf("%u. ", (sviDijeloviO+indeksPronalaska)->id+1);
					printf("%s\t", (sviDijeloviO + indeksPronalaska)->naziv);        
					printf("%u\t", (sviDijeloviO + indeksPronalaska)->sifra);
					printf("%s\t", (sviDijeloviO + indeksPronalaska)->jm);
					printf("%d\t\n", (sviDijeloviO + indeksPronalaska)->stanje);
				}
				else
				{
					printf("Ne postoji taj dio.\n");
				}
				free(sviDijeloviO);
			}
		}
	}
}

void pretraziAlat(char* datotekaA, unsigned int* pBrojDijelovaA)
{
	FILE* pDatotekaProcitajA = NULL;
	pDatotekaProcitajA = fopen(datotekaA, "rb");
	if (pDatotekaProcitajA == NULL)
	{
		perror("Izbornik 3 - Pregled skladista");
		return;
	}
	else
	{
		DIJELOVI* sviDijeloviA = NULL;
		fread(pBrojDijelovaA, sizeof(int), 1, pDatotekaProcitajA);
		if (*pBrojDijelovaA == 0)
		{
			printf("Nema unesenih dijelova.\n");
			fclose(pDatotekaProcitajA);
			return;
		}
		else
		{
			sviDijeloviA = (DIJELOVI*)calloc(*pBrojDijelovaA, sizeof(DIJELOVI));
			if (sviDijeloviA == NULL)
			{
				perror("Citanje svih dijelova");
				exit(EXIT_FAILURE);
			}
			else
			{
				fread(sviDijeloviA, sizeof(DIJELOVI), *pBrojDijelovaA, pDatotekaProcitajA);
				fclose(pDatotekaProcitajA);
				unsigned int i;
				char privremeniNaziv[51] = { '\0' };
				unsigned int statusPronalaska = 0;
				unsigned int indeksPronalaska = -1;
				printf("Unesite trazeni dio: ");
				scanf(" %50[^\n]", privremeniNaziv);
				for (i = 0; i < *pBrojDijelovaA; i++)
				{
					if (!strcmp((sviDijeloviA + i)->naziv, privremeniNaziv))
					{
						statusPronalaska = 1;
						indeksPronalaska = i;
					}
				}
				if (statusPronalaska)
				{
					printf("Dio pronadjen\n");
					printf("NAZIV\t\t\tSIFRA\t\tJM\tSTANJE\n");
					printf("%u. ", (sviDijeloviA + indeksPronalaska)->id+1);
					printf("%s\t", (sviDijeloviA + indeksPronalaska)->naziv);
					printf("%u\t", (sviDijeloviA + indeksPronalaska)->sifra);
					printf("%s\t", (sviDijeloviA + indeksPronalaska)->jm);
					printf("%d\t\n", (sviDijeloviA + indeksPronalaska)->stanje);
				}
				else
				{
					printf("Ne postoji taj dio.\n");
				}
				free(sviDijeloviA);
			}
		}
	}
}

void pretraziRezervu(char* datotekaR, unsigned int* pBrojDijelovaR)
{
	FILE* pDatotekaProcitajR = NULL;
	pDatotekaProcitajR = fopen(datotekaR, "rb");
	if (pDatotekaProcitajR == NULL)
	{
		perror("Izbornik 3 - Pregled skladista");
		return;
	}
	else
	{
		DIJELOVI* sviDijeloviR = NULL;
		fread(pBrojDijelovaR, sizeof(unsigned int), 1, pDatotekaProcitajR);
		if (*pBrojDijelovaR == 0)
		{
			printf("Nema unesenih dijelova.\n");
			fclose(pDatotekaProcitajR);
			return;
		}
		else
		{
			sviDijeloviR = (DIJELOVI*)calloc(*pBrojDijelovaR, sizeof(DIJELOVI));
			if (sviDijeloviR == NULL)
			{
				perror("Citanje svih dijelova");
				exit(EXIT_FAILURE);
			}
			else
			{
				fread(sviDijeloviR, sizeof(DIJELOVI), *pBrojDijelovaR, pDatotekaProcitajR);
				fclose(pDatotekaProcitajR);
				unsigned int i;
				char privremeniNaziv[51] = { '\0' };
				unsigned int statusPronalaska = 0;
				unsigned int indeksPronalaska = -1;
				printf("Unesite trazeni dio: ");
				scanf(" %50[^\n]", privremeniNaziv);
				for (i = 0; i < *pBrojDijelovaR; i++)
				{
					if (!strcmp((sviDijeloviR + i)->naziv, privremeniNaziv))
					{
						statusPronalaska = 1;
						indeksPronalaska = i;
					}
				}
				if (statusPronalaska)
				{
					printf("Dio pronadjen\n");
					printf("NAZIV\t\t\tSIFRA\t\tJM\tSTANJE\n");
					printf("%u. ", (sviDijeloviR + indeksPronalaska)->id+1);
					printf("%s\t", (sviDijeloviR + indeksPronalaska)->naziv);
					printf("%u\t", (sviDijeloviR + indeksPronalaska)->sifra);
					printf("%s\t", (sviDijeloviR + indeksPronalaska)->jm);
					printf("%d\t\n", (sviDijeloviR + indeksPronalaska)->stanje);
				}
				else
				{
					printf("Ne postoji taj dio.\n");
				}
				free(sviDijeloviR);
			}
		}
	}
}

void brisanjeDatotekeZ(char* datotekaZ)
{
	FILE* obrisiZ = NULL;
	int status = 0;
	obrisiZ = fopen(datotekaZ, "r");
	if (obrisiZ == NULL)
	{
		printf("Datoteka ne postoji na disku\n");
		return;
	}
	else
	{
		fclose(obrisiZ);
	}
	status = remove(datotekaZ);
	if (status == 0)
	{
		printf("Uspjesno obrisana datoteka!\n");
	}
	else
	{
		printf("Nemogucnost brisanja datoteke!\n");
	}
}

void brisanjeDatotekeA(char* datotekaA)
{
	FILE* obrisiA = NULL;
	int status = 0;
	obrisiA = fopen(datotekaA, "r");
	if (obrisiA == NULL)
	{
		printf("Datoteka ne postoji na disku\n");
		return;
	}
	else
	{
		fclose(obrisiA);
	}
	status = remove(datotekaA);
	if (status == 0)
	{
		printf("Uspjesno obrisana datoteka!\n");
	}
	else
	{
		printf("Nemogucnost brisanja datoteke!\n");
	}
}

void brisanjeDatotekeR(char* datotekaR)
{
	FILE* obrisiR = NULL;
	int status = 0;
	obrisiR = fopen(datotekaR, "r");
	if (obrisiR == NULL)
	{
		printf("Datoteka ne postoji na disku\n");
		return;
	}
	else
	{
		fclose(obrisiR);
	}
	status = remove(datotekaR);
	if (status == 0)
	{
		printf("Uspjesno obrisana datoteka!\n");
	}
	else
	{
		printf("Nemogucnost brisanja datoteke!\n");
	}
}

void fIzlazakIzPrograma(void)
{
	printf("Da li ste sigurni da zelite zavrsiti program?\n");
	char izbor[5] = { '\0' };
	scanf(" %2s", izbor);
	if (!strcmp("da", izbor) || !strcmp("DA", izbor) || !strcmp("Da", izbor) || !strcmp("dA", izbor))
	{
		exit(EXIT_FAILURE);
	}
	return;
}

void fIzbornik(char* datotekaZ, char* datotekaA, char* datotekaR, unsigned int* pBrojDijelovaO, unsigned int* pBrojDijelovaA, unsigned int* pBrojDijelovaR)
{
	int izbornik = -1;
	int skladiste = -1;
	do
	{
		system("cls");
		printf("------------------------------------------\n");
		printf("|               IZBORNIK                 |\n");
		printf("------------------------------------------\n");
		printf("| 1 - Pregled skladista                  |\n");
		printf("| 2 - Unos novih dijelova u skladiste    |\n");
		printf("| 3 - Pretrazivanje dijelova po nazivu   |\n");
		printf("| 4 - Brisanje datoteke                  |\n");
		printf("| 5 - Izlazak iz programa                |\n");
		printf("------------------------------------------\n");
		printf("Unesi izbor: ");
		scanf("%d", &izbornik);
		system("cls");
		switch (izbornik)
		{
		case 1:
			do {
				printf("---------------------------------------------------\n");
				printf("| 1 - Pregled kategorije zastitne odjece i opreme |\n");
				printf("| 2 - Pregled kategorije alata                    |\n");                                //ukoliko se odabere opcija 1 otvara se jo� jedan izbornik s 3 kategorije
				printf("| 3 - Pregled kategorije rezervnih dijelova       |\n");
				printf("| 0 - Povratak na glavni izbornik                 |\n");
				printf("---------------------------------------------------\n");
				printf("Unesi izbor: ");
				scanf("%d", &skladiste);
				system("cls");
				switch (skladiste)
				{
				case 1:
					pregledOpreme(datotekaZ, pBrojDijelovaO);
					break;
				case 2:
					pregledAlata(datotekaA, pBrojDijelovaA);
					break;
				case 3:
					pregledRezerve(datotekaR, pBrojDijelovaR);
					break;
				case 0:
					fIzbornik(datotekaZ, datotekaA, datotekaR, pBrojDijelovaO, pBrojDijelovaA, pBrojDijelovaR);
					break;
				}
			} while (1);
			break;
		case 2: 
			do {
				printf("---------------------------------------------------\n");
				printf("| 1 - Unos u kategoriju zastitne odjece i opreme  |\n");
				printf("| 2 - Unos u kategoriju alata                     |\n");
				printf("| 3 - Unos u kategoriju rezervnih dijelova        |\n");
				printf("| 0 - Povratak na glavni izbornik                 |\n");
				printf("---------------------------------------------------\n");
				printf("Unesi izbor: ");
				scanf("%d", &skladiste);
				system("cls");
				switch (skladiste)
				{
				case 1:
					fDodajOpremu(datotekaZ, pBrojDijelovaO);
					break;
				case 2:
					fDodajAlat(datotekaA, pBrojDijelovaA);
					break;
				case 3:
					fDodajRezervu(datotekaR, pBrojDijelovaR);
					break;
				case 0:
					fIzbornik(datotekaZ, datotekaA, datotekaR, pBrojDijelovaO, pBrojDijelovaA, pBrojDijelovaR);
					break;
				}
			} while (1); 
			break;
		case 3:
			do {
				printf("---------------------------------------------------------\n");
				printf("| 1 - Pretrazivanje kategorije zastitne odjece i opreme |\n");
				printf("| 2 - Pretrazivanje kategorije alata                    |\n");
				printf("| 3 - Pretrazivanje kategorije rezervnih dijelova       |\n");
				printf("| 0 - Povratak na glavni izbornik                       |\n");
				printf("---------------------------------------------------------\n");
				printf("Unesi izbor: ");
				scanf("%d", &skladiste);
				system("cls");
				switch (skladiste)
				{
				case 1:
					pretraziOpremu(datotekaZ, pBrojDijelovaO);
					break;
				case 2:
					pretraziAlat(datotekaA, pBrojDijelovaA);
					break;
				case 3:
					pretraziRezervu(datotekaR, pBrojDijelovaR);
					break;
				case 0:
					fIzbornik(datotekaZ, datotekaA, datotekaR, pBrojDijelovaO, pBrojDijelovaA, pBrojDijelovaR);
					break;
				}
			} while (1);
			break;
		case 4:
			do {
				printf("-------------------------------------------------------------\n");
				printf("| 1 - Brisanje datoteke kategorije zastitne odjece i opreme |\n");
				printf("| 2 - Brisanje datoteke kategorije alata                    |\n");
				printf("| 3 - Brisanje datoteke kategorije rezervnih dijelova       |\n");
				printf("| 0 - Povratak na glavni izbornik                           |\n");
				printf("-------------------------------------------------------------\n");
				printf("Unesi izbor: ");
				scanf("%d", &skladiste);
				system("cls");
				switch (skladiste)
				{
				case 1:
					brisanjeDatotekeZ(datotekaZ);
					break;
				case 2:
					brisanjeDatotekeA(datotekaA);
					break;
				case 3:
					brisanjeDatotekeR(datotekaR);
					break;
				case 0:
					fIzbornik(datotekaZ, datotekaA, datotekaR, pBrojDijelovaO, pBrojDijelovaA, pBrojDijelovaR);
					break;
				}
			} while (1);
			break;
		case 5:
			fIzlazakIzPrograma();
			break;
		default:
			printf("Krivo odabrana opcija, pokusajte ponovno\n");
		}
	} while (1);
}