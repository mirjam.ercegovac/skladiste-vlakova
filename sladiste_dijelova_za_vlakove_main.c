#include<stdio.h>
#include"skladiste_dijelova_za_vlakove_head.h"

int main(void)
{
	char* dijeloviZ = "oprema.bin";
	char* dijeloviA = "alat.bin";
	char* dijeloviR = "rezervni.bin";
    unsigned int brojDijelovaO = 0;
	unsigned int brojDijelovaA = 0;
	unsigned int brojDijelovaR = 0;

	fProvjeraDatotekeZ(dijeloviZ, &brojDijelovaO);
	fProvjeraDatotekeA(dijeloviA, &brojDijelovaA);
	fProvjeraDatotekeR(dijeloviR, &brojDijelovaR);
	fIzbornik(dijeloviZ, dijeloviA, dijeloviR, &brojDijelovaO, &brojDijelovaA, &brojDijelovaR);
	
	return 0;
}