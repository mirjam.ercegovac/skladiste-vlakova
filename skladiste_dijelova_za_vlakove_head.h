#ifndef FUNCTIONS_H
#define FUNCTIONS_H

typedef struct dijelovi {
	char naziv[51];
	unsigned int sifra;
	char jm[11];
	int stanje;
	unsigned int id;
}DIJELOVI;

void pregledOpreme(char*, unsigned int*);
void pregledAlata(char*, unsigned int*);
void pregledRezerve(char*, unsigned int*);
void fProvjeraDatotekeZ(char*, unsigned int*);
void fProvjeraDatotekeA(char*, unsigned int*);
void fProvjeraDatotekeR(char*, unsigned int*);
void fDodajOpremu(char*, unsigned int*);
void fDodajAlat(char*, unsigned int*);
void fDodajRezervu(char*, unsigned int*);
void pretraziOpremu(char*, unsigned int*);
void pretraziAlat(char*, unsigned int*);
void pretraziRezervu(char*, unsigned int*);
void brisanjeDatotekeZ(char*);
void brisanjeDatotekeA(char*);
void brisanjeDatotekeR(char*);
void fIzlazakIzPrograma(void);
void fIzbornik(char*, char*, char*, unsigned int*, unsigned int*, unsigned int*);

#endif // !FUNCTIONS_H

